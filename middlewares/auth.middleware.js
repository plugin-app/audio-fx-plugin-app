const isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
        const error = new Error('You need to be logged in to have access');
        return res.json(error.message);
    }
};

const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role === 'admin') {
            next();
        } else {
            const error = new Error('You\'ll need admin permission');
            return res.status(403).json(error.message);
        }
    }
};

module.exports = {isAuthenticated, isAdmin};
