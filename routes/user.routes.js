const express = require('express');
const {upload, uploadToCloudnary} = require('../middlewares/file.middleware');
const authMiddleware = require('../middlewares/auth.middleware');
const userController = require('../controllers/user.controller');

const router = express.Router();

router.get('/info', userController.infoGet);

router.get('/user', userController.userGet);

router.put('/edit', [upload.single('image'), uploadToCloudnary], userController.editPut );

router.get('/all', [authMiddleware.isAdmin], userController.allGet);

router.put('/role', [authMiddleware.isAdmin], userController.rolePut );

router.put('/follow', userController.followersPut);

router.put('/unfollow', userController.followersDelete);

module.exports = router;
