const express = require('express');
const {upload, uploadToCloudnary} = require('../middlewares/file.middleware');
const authMiddleware = require('../middlewares/auth.middleware');
const pluginsContoller = require('../controllers/plugins.controller');

const router = express.Router();

router.get('/all', pluginsContoller.allGet);

router.get('/platform/:platform', pluginsContoller.platformGet);

router.get('/category/:category', pluginsContoller.categoryGet);

router.get('/type/:type', pluginsContoller.typeGet);

router.get('/:id', pluginsContoller.pluginById);

router.post(
    '/create',
    [upload.single('image'), uploadToCloudnary, authMiddleware.isAuthenticated],
    pluginsContoller.createPost);

router.put(
    '/edit',
    [upload.single('image'), uploadToCloudnary, authMiddleware.isAuthenticated],
    pluginsContoller.editPut);

router.post('/rate', pluginsContoller.ratePost);

router.put('/edit-rate', pluginsContoller.editRatePut);

module.exports = router;
