const express = require('express');
const authController = require('../controllers/auth.controller');

const router = express.Router();

router.post('/register', authController.registerPost);

router.post('/login', authController.loginPost);

router.get('/check-session', authController.checkSession);

router.post('/logout', authController.logoutPost);


module.exports = router;
