require('dotenv').config();
const express = require('express');
const passport = require('passport');
const session = require('express-session');
const db = require('./db');
const MongoStore = require('connect-mongo');
const path = require('path');
const methodOverride = require('method-override');
const cors = require('cors');

const indexRoutes = require('./routes/index.routes');
const authRoutes = require('./routes/auth.routes');
const pluginsRoutes = require('./routes/plugins.routes');
const userRoutes = require('./routes/user.routes');

const authMiddleware = require('./middlewares/auth.middleware');

require('./passport/passport');

db.connect();

const app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT');
    res.header('Access-Control-Allow-Credentials', true);
    res.header(
        'Access-Control-Allow-Headers',
        'Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token',
    );
    next();
});

app.use(
    cors({
        origin: ['http://localhost:3000', ' https://plugin-app.netlify.app/'],
        credentials: true,
    }),
);

const {PORT} = process.env || 3000;
const {SESSION_SECRET} = process.env;

app.use(
    session({
        secret: SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 48 * 60 * 60 * 1000,
        },
        store: MongoStore.create({mongoUrl: db.DB_URL}),
    }),
);

app.use(passport.initialize());
app.use(passport.session());

app.use(methodOverride('_method'));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/plugins', pluginsRoutes);
app.use('/user', [authMiddleware.isAuthenticated], userRoutes);

app.use('*', (req, res, next) => {
    const error = new Error('Route not Found');
    console.log(error);
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    error.message = error.message || 'Unexpected error';
    return res.status(error.status || 500).json(error.message);
});

// -------------SERVER CONFIG ------------------------------------
const serverCallback = () => {
    console.log(`Servidor escuchando en http://localhost:${PORT}`);
};

app.listen(PORT, serverCallback);
